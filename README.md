# Oneup Front-end

For testing the api go to this url [http://178.62.123.225/](http://178.62.123.225/)

# OR clone app locally 

1. Clone the app using the following command `git clone git@github.com:mateeyow/oneup-front.git`
2. Install serve in your global node_modules `npm install -g serve`
3. Run the server `serve`

Got to the app location: [http://localhost:3000/](http://localhost:3000/)

1. Click the `SignIn Using Auth0` button
2. Signup new user
3. Click the `Check Api` button
4. You should get an alert with message `We got the secured data sucessfully`
5. Test the donation, populate the input field with the following data
	- Card Number: 4242424242424242
	- Expiry: 05/2019
	- Name on Card: Test
	- CVC: 123
6. Logout