angular.module( 'oneup.home', ['auth0', 'angularPayments'])
.controller( 'HomeCtrl', function HomeController( $scope, auth, $http, $location, store ) {

  $scope.auth = auth;

  $scope.callApi = function() {
    // Just call the API as you'd do using $http
    $scope.isCalled = true;
    $http({
      url: 'https://oneup-testing.herokuapp.com/secure',
      method: 'GET'
    }).then(function() {
      $scope.isCalled = false;
      alert("We got the secured data successfully");
    }, function(response) {
      $scope.isCalled = false;
      console.log(response);
      if (response.status == 0) {
        alert("Please download the API seed so that you can call it.");
      }
      else {
        alert(response.data);
      }
    });
  }

  $scope.logout = function() {
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $location.path('/login');
  }

  $scope.handleStripe = function (status, response) {
    $scope.isLoading = true;
    var token = response.id;
    $http.post('https://oneup-testing.herokuapp.com/api/v1/donate', {stripeToken: token})
    .then(function (response) {
      $scope.isLoading = false;
      $scope.isSuccess = true;
    })
  };
});
